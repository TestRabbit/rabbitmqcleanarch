using CleanArch.Application.Interface;
using CleanArch.Application.Services;
using CleanArch.Core;
using CleanArch.Infras.Repositories;
using CleanArch.Infras.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using ServiceStack.Redis;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Unity;
using Unity.Lifetime;

namespace CleanArch.App
{


    public class SendDapperRepository
    {

        private readonly IPersonRepository _personRepository;
 
        public SendDapperRepository(IPersonRepository personRepository )
        {
            this._personRepository = personRepository;
        }

        public async Task<int> AddAsync(Person person)
        {
            var data =  await  _personRepository.AddAsync(person);
            return  (data);
        }
       

    }
    public class SentRedisRepository
    {
        private readonly IPersonRepositoryRedis _personRepositoryRedis;
        public SentRedisRepository(IPersonRepositoryRedis personRepositoryRedis)
        {
            _personRepositoryRedis = personRepositoryRedis;
        }
        public void AddAsync(Person person)
        {
           _personRepositoryRedis.AddAsync(person);  
        }
    }
    public class SetLogg
    {
        private readonly ILoggerService _loggerService;
        public SetLogg(ILoggerService loggerService )
        {
            _loggerService = loggerService;
        }
        public void LoggPerson(Person person)
        {
            _loggerService.LogPerson(person);
        }
        public void LoggInof(string message)
        {
            _loggerService.LogInfo(message);
        }

        public void LoggError(string message)
        {
            _loggerService.LogError(message);
        }

    }


    public class Program
    {
        
        public static void Main(string[] args)
        {
            //Run Service
            CreateHostBuilder(args).Build().Run();
            

            //Create object  Dapper
            SendDapperRepository   sendDapperRepository = new SendDapperRepository(new PersonRepository());

            //Create object Redis
            SentRedisRepository sentRedisRepository = new SentRedisRepository(new PersonRepositoryRedis());
            
            //Craete object Serilog
            SetLogg logers = new SetLogg(new LoggerService());

            logers.LoggInof(" Services is Run.");

            //Connection To Rabbit  and insert data 
            var factory = new ConnectionFactory() { HostName = "localhost" };
           
            try
            {
                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                    {
                        channel.QueueDeclare(queue: "Object", durable: false, exclusive: false, autoDelete: false, arguments: null);
                        var consumer = new EventingBasicConsumer(channel);
                        consumer.Received += (model, ea) =>
                        {
                            var body = ea.Body.ToArray();
                            Person p = (Person)Service.Desserialized.Desserializ(body);

                            //dapper insert
                            Person person = new Person() { Name = p.Name, LastName = p.LastName, Age = p.Age };
                            _ = sendDapperRepository.AddAsync(person);
                            //redis insert  
                            sentRedisRepository.AddAsync(person);
                            //log info 
                            logers.LoggPerson(person);

                        };

                        channel.BasicConsume(queue: "Object", autoAck: true, consumer: consumer);
                        logers.LoggInof("Wait for Recived.... Press [enter] to exit.");
                        Console.ReadKey();
                    }
                }
                catch (Exception ex )
                {

              logers.LoggError("error connention Rabbit or Insert Data to Database,Please chech it.");
                Console.ReadKey();
            }
           
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<Worker>();
                    RegisterServices(services);
                });


        public static void RegisterServices(IServiceCollection services)
        {
            DependencyCountainer.RegisterServices(services);
          
        }
    }


    
       
    
}
