﻿using CleanArch.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CleanArch.App.Service
{
   public static  class Desserialized
    {
        public static Person Desserializ(byte[] data)
        {
            Person p = new Person();
            using (MemoryStream m = new MemoryStream(data))
            {
                using (BinaryReader reader = new BinaryReader(m))
                {
                    p.Name = reader.ReadString();
                    p.LastName = reader.ReadString();
                    p.Age = reader.ReadInt32();
                }
            }
            return p;
        }
    }
}
