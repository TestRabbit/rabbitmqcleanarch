﻿using CleanArch.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CleanArch.Application.Interface
{
    public interface ILoggerService
    {
        public void LogPerson(Person person);
        public void LogInfo(string message);
        public void LogError(string message);
    }
}
