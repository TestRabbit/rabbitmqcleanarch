﻿using CleanArch.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CleanArch.Application.Interface
{
   public interface IPersonRepository  
    {
        Task<int> AddAsync(Person person);
    }
}
