﻿using CleanArch.Application.Interface;
using CleanArch.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;
using Serilog.Formatting.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CleanArch.Application.Services
{
    public class LoggerService : Person, ILoggerService 
    {
         public LoggerService()
        {
            Log.Logger = new LoggerConfiguration()
                 .WriteTo.Console()
                 .WriteTo.File(new JsonFormatter(), @"sLogger.json")
                 .WriteTo.File(@"sLogger-.logs", rollingInterval: RollingInterval.Day)
                 .MinimumLevel.Debug()
                 .CreateLogger();
        }

        public void LogError(string message)
        {
            Log.Error(message);
        }

        public void LogInfo(string message)
        {
            Log.Information(message);
        }

        public  void LogPerson(Person person)
        {
            Name =  person.Name;
            LastName = person.LastName;
            Age = person.Age;

              Log.Information("Insert a person{@person}  at {now} ", this, DateTime.Now);
        }

    }
}
