﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CleanArch.Core
{
   public class Person
    {
        private string name;
        private string lastName;
        private int age;


        public string  Name{  get { return name; } set { name = value; }}
        public string LastName  { get {return lastName ; }set { lastName = value; }}
        public int Age {get { return age; }set{if (value >= 50) age = 0; else age = value;}}

       

    }
}
