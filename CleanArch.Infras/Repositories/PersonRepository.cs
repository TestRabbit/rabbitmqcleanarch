﻿using CleanArch.Application.Interface;
using CleanArch.Core;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace CleanArch.Infras.Repositories
{
   public class PersonRepository : IPersonRepository
    {
 
        public async Task<int> AddAsync(Person person)
        {
             
           var sqlQuery = "Insert Into Persons (Name,LastName,Age) Values (@Name,@LastName,@Age)";
           using (var connection = new SqlConnection("Data Source= .; Initial Catalog=ConfigDB ; User ID=sa ;Password=Pardaz_1ppco;TrustServerCertificate=True "))
            {
                connection.Open();
                var result = await connection.ExecuteAsync(sqlQuery, person);
                return result;
            }
        }
    }
}
