﻿using CleanArch.Application.Interface;
using CleanArch.Core;
using ServiceStack.Redis;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace CleanArch.Infras.Repositories
{
    public class PersonRepositoryRedis : IPersonRepositoryRedis
    {
        public bool AddAsync(Person person)
        {
            try
            {
                var json = JsonSerializer.Serialize(person);
                Guid nGuid = Guid.NewGuid();
                using (var connectionRedis = new RedisClient())
                {
                    connectionRedis.Set(nGuid.ToString(), json);
                }
                return true;
            }
            catch 
            {

                return false;
            }
        }
    }

    
}
