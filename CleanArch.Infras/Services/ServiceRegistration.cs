﻿using CleanArch.Application.Interface;
using CleanArch.Application.Services;
using CleanArch.Infras.Repositories;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace CleanArch.Infras.Services
{
   public static class DependencyCountainer
    {
        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddTransient<IPersonRepository, PersonRepository>();
            services.AddTransient<ILoggerService, LoggerService>();
            services.AddTransient<IPersonRepositoryRedis, PersonRepositoryRedis>();

        }
    }
}
